DESCRIPTION = "Telemetry monitoring application"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://telemetry.c \
           file://telemetry.conf \
           file://telemetry.init"

S = "${WORKDIR}"

inherit update-rc.d

INITSCRIPT_NAME = "telemetry"
INITSCRIPT_PARAMS = "defaults"

do_compile() {
    ${CC} ${CFLAGS} ${LDFLAGS} -o telemetry telemetry.c -pthread
}

do_install() {
    install -d ${D}${bindir}
    install -m 0755 telemetry ${D}${bindir}
    install -d ${D}${sysconfdir}
    install -m 0644 telemetry.conf ${D}${sysconfdir}
    install -d ${D}${sysconfdir}/init.d
    install -m 0755 telemetry.init ${D}${sysconfdir}/init.d/telemetry
}

FILES_${PN} += "${sysconfdir}/telemetry.conf ${sysconfdir}/init.d/telemetry"
